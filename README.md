# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Demonstrate various use cases for entity reference fields. Several views exist to show differences in the use of referenced content and referencing content. The project is maintained as a Drupal feature module.
* Version 7.x-0.1

### How do I get set up? ###

* Copy archive to a Drupal sites/all/modules directory.
* Enable the ER_Laboratory feature.
* Dependencies: Drupal 7.x
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact