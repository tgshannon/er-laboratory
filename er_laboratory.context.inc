<?php
/**
 * @file
 * er_laboratory.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function er_laboratory_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'article';
  $context->description = 'Article context';
  $context->tag = 'erlab';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'article' => 'article',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-1' => array(
          'module' => 'block',
          'delta' => '1',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-parent_parent_link_-block_1' => array(
          'module' => 'views',
          'delta' => 'parent_parent_link_-block_1',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'views-siblings-block' => array(
          'module' => 'views',
          'delta' => 'siblings-block',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'views-children-block' => array(
          'module' => 'views',
          'delta' => 'children-block',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
        'views-peer-block_1' => array(
          'module' => 'views',
          'delta' => 'peer-block_1',
          'region' => 'sidebar_first',
          'weight' => '-6',
        ),
        'block-2' => array(
          'module' => 'block',
          'delta' => '2',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-parent_parent_link_-block' => array(
          'module' => 'views',
          'delta' => 'parent_parent_link_-block',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-siblings-block_1' => array(
          'module' => 'views',
          'delta' => 'siblings-block_1',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'views-children-block_1' => array(
          'module' => 'views',
          'delta' => 'children-block_1',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
        'views-peer-block' => array(
          'module' => 'views',
          'delta' => 'peer-block',
          'region' => 'sidebar_second',
          'weight' => '-6',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Article context');
  t('erlab');
  $export['article'] = $context;

  return $export;
}
